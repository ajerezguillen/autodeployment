from fabric.api import *
from fabric.context_managers import settings
from fabric.network import ssh
# 
from cuisine import *

import os
import user
import socket

sys.path.append("conf/")
import conf
import superfichas
import httpd
import git
