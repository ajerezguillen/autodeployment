from imports import *


def setup():

    #dir_ensure('/usr/share/ca-certificates/superfichas/')
    #env.user = 'root'
    sudo('mkdir %s %s' % (True and "-p" or "", '/etc/httpd/sites-enabled/'))
    sudo('mkdir %s %s' % (True and "-p" or "", '/etc/httpd/sites-available/'))
    #dir_ensure('/etc/httpd/sites-enabled/')
    #dir_ensure('/etc/httpd/sites-available/')
    #run("sudo a2enmod rewrite")
    #run("sudo a2enmod headers")
    #run("sudo a2enmod fastcgi")
    #run("sudo a2enmod alias")
    #run("sudo a2enmod actions")
    #run("sudo a2enmod ssl")
    #put('../conf/web0x/nginx/ssl/superfichas/', '/usr/share/ca-certificates/')
    #put('../conf/sp0x/apache2/sites-available/sp-*', '/etc/apache2/sites-available/')
    #put('../conf/sp0x/apache2/.htpasswd', '/etc/apache2/.htpasswd')
    #put('../conf/sp0x/apache2/*.conf', '/etc/apache2/')
    #put("web0x/conf/nginx/fastcgi_cache", "/etc/nginx/fastcgi_cache")
    #put("web0x/conf/nginx/fastcgi_api_cache", "/etc/nginx/fastcgi_api_cache")
    #put("web0x/conf/nginx/fastcgi_params", "/etc/nginx/fastcgi_params")
    #put("web0x/conf/nginx/nginx.conf", "/etc/nginx/nginx.conf")
    #dir_ensure("/var/lib/nginx/cache/cache_prod", True)
    #dir_ensure("/var/lib/nginx/cache/cache_testing", True)

    upstart_restart('httpd')

def create_site(vhostname, path, example_file=conf.web_example_file, priority="050", ssl=False ):
    variables = conf.http_conf_variables
    variables = {
        '{{servername}}':vhostname,
        '{{root}}':path + '/public/;',
        '{{path}}':path,
        #'{{root}}':'/var/www/vhosts/' + vhostname + '/public/;',
        '{{dbhost}}':'127.0.0.1'
    }
    
    local_file = example_file
    if ssl :
        config_file = "/etc/httpd/sites-available/ssl-" + vhostname
    else:
        config_file = "/etc/httpd/sites-available/" + vhostname


    put(local_file, config_file, use_sudo=True)


    for key, value in variables.items():
        fabric.contrib.files.sed(config_file, re.escape(key), str(value), use_sudo=True)

    if ssl :
        enable_site("ssl-" + vhostname, priority)
    else :
        enable_site(vhostname, priority)

        
def enable_site(vhostname, priority):
    mode_sudo()
    file_link('/etc/httpd/sites-available/%s' % (vhostname), '/etc/httpd/sites-enabled/%s-%s.conf' % (priority, vhostname))
    upstart_restart('httpd')    

def disable_site(vhostname, priority):
    mode_sudo()
    file_unlink('/etc/httpd/sites-enabled/%s-%s' % (priority, vhostname), use_sudo=True)
    upstart_restart('httpd')

def remove_site(vhostname, path, priority):
    mode_sudo()
    disable_site(vhostname)
    dir_remove(path, use_sudo=True)
    upstart_restart('httpd')