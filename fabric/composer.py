from imports import *
import imports
import conf
import fabfile
import cuisine

def update(path):
    with cd(path):
        run("php composer.phar update")

def install(path):
    with cd(path):
        run("php composer.phar install")

def setup(path):
    with cd(path):
        sudo("curl -sS https://getcomposer.org/installer | php")
        #run("mv composer.phar /usr/local/bin/composer")