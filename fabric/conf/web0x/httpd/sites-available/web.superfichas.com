<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/vhosts/{{servername}}/public/
    ServerName {{servername}}

    RewriteEngine On
    RewriteCond %{HTTP_HOST} =superfichas.com [NC]
    RewriteRule ^(.*)$ http://{{servername}}$1 [L,R=301]

    RewriteCond %{HTTP_HOST} !={{servername}} [NC]
    RewriteRule ^(.*)$ - [L,R=404]

    <Directory />
        Options +FollowSymLinks
        AllowOverride All
        #Include auth.conf
    </Directory>
    <Directory /var/www/vhosts/{{servername}}>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>

    ErrorLog /var/log/httpd/error.log

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    CustomLog /var/log/httpd/{{servername}}-access.log combined
    #CustomLog /dev/null combined

    <FilesMatch "\.(ico|pdf|flv|jpg|jpeg|png|gif|js|css|swf)$">
        Header set Cache-Control "max-age=290304000, public"
    </FilesMatch>

</VirtualHost>
